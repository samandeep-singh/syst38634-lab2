package lab2;

import java.util.Scanner;

public class View {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		NumberOperations operator = new NumberOperations();
		StringOperations strOperator = new StringOperations();
		int[] userInput = new int[3];
		String userInpStr;
		int index;
		/*
		for(int i = 0; i < 3; i++) {
			System.out.print("Input number " + (i+1) + " : ");
			userInput[i] = scan.nextInt();
		}
		*/
		System.out.print("Input a string : ");
		userInpStr = scan.nextLine();
		scan.close();
		
		//System.out.println("\nThe smallest value is : " + operator.findSmallest(userInput));
		//System.out.println("\nThe average value is: " + operator.findAverage(userInput));
		//System.out.println("\nThe character at position " + index +" is : " + strOperator.getCharAtIndex(userInpStr, index));
		System.out.println("\nCodepoint count = " + strOperator.getUnicodePoints(userInpStr));
	}

}
