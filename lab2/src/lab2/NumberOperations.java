package lab2;

public class NumberOperations {
	
	public int findSmallest(int[] numArray) {
		
		int smallest = numArray[0];
		for(int i = 0; i < numArray.length; i++) {
			if(smallest > numArray[i]) {
				smallest = numArray[i];
			}
		}
		return smallest;
		
	}
	
	public double findAverage(int[] numArray) {
		
		double sum = 0.0;
		for(int i = 0; i < numArray.length; i++) {
			sum += (double)numArray[i];
		}
		return sum/numArray.length;
	}
	
	public String getMiddleChar(String str) {
		
		String result = "";
		if(str.length() % 2 == 0) {
			result += Character.toString(str.charAt((str.length()/2) - 1));
			result += Character.toString(str.charAt(str.length()/2));	
		}
		else {
			result += Character.toString(str.charAt((str.length()/2)));
		}
		return result;
		
	}

}
