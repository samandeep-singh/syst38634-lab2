package lab2;

public class StringOperations {
	
	public int countVowels(String str) {
		
		str = str.toLowerCase();
		char[] vowels = {'a', 'e', 'i', 'o', 'u'};
		int vowelCounter = 0;
		for(int i = 0; i < str.length(); i++) {
			for(char c : vowels) {
				if (c == str.charAt(i)) vowelCounter++;
			}
		}
		return vowelCounter;
		
	}
	
	public char getCharAtIndex(String str, int index) {
		
		return str.charAt(index);
		
	}
	
	public int getUnicodePoints(String str) {
		
		return str.codePointCount(0, str.length());
		
	}

}
